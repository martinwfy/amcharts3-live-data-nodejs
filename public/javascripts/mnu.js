(function(){

    var button = document.getElementById('cn-button'),
        wrapper = document.getElementById('cn-wrapper'),
        overlay = document.getElementById('cn-overlay');

    //open and close menu when the button is clicked
    var open = false;
    button.addEventListener('click', handler, false);
    wrapper.addEventListener('click', cnhandle, false);
    button.innerHTML = "+";

    function cnhandle(e){
        e.stopPropagation();
        e = [].slice.call(e.target.childNodes);

        setTimeout(function(){
            location.href='/app/'+e[0].id;

        },100);
    }

    function handler(e){
        if (!e) var e = window.event;
        e.stopPropagation();//so that it doesn't trigger click event on document
        changeOpn();
    }
    function changeOpn(){
        if(!open){
            openNav();
        }
        else{
            closeNav();
        }
    }
    function openNav(){
        open = true;
        button.innerHTML = "-";
        classie.add(overlay, 'on-overlay');
        classie.add(wrapper, 'opened-nav');
    }
    function closeNav(){
        open = false;
        button.innerHTML = "+";
        classie.remove(overlay, 'on-overlay');
        classie.remove(wrapper, 'opened-nav');
    }
    document.addEventListener('click', closeNav);

})();

$(".container").removeClass("contenidoPrincipal");

