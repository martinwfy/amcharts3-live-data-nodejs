$(document).ready(function() {
  // Handler for .ready() called.

  var socket = io.connect("http://localhost:9000"); //io('http://localhost:9000');

  //Envio de mensaje de prueba
  var numeroPrueba = Math.floor(Math.random() * 500 + 100);
  setInterval(function() {
    socket.emit("actualiza", numeroPrueba);
  }, 1000);

  var ultimoAnio = 1994;

  //Datos
  var dataJSON = [
    {
      year: "1990",
      value: 500
    },
    {
      year: "1991",
      value: 800
    },
    {
      year: "1992",
      value: 1000
    },
    {
      year: "1993",
      value: 327
    },
    {
      year: "1994",
      value: 70
    },
    {
      year: "1993",
      value: 327
    },
    {
      year: "1994",
      value: 70
    }
  ];

  //Grafica 1

  var chart1 = AmCharts.makeChart("chartdiv", {
    type: "serial",
    theme: "light",
    backgroundColor: "FFFFFF",
    marginTop: 20,
    marginRight: 20,
    dataProvider: dataJSON,
    valueAxes: [
      {
        axisAlpha: 0,
        position: "left"
      }
    ],
    graphs: [
      {
        id: "g1",
        balloonText:
          "[[category]]<br><b><span style='font-size:14px;'>[[value]]</span></b>",
        bullet: "round",
        bulletSize: 6,
        lineColor: "#d1655d",
        lineThickness: 2,
        negativeLineColor: "#637bb6",
        type: "smoothedLine",
        valueField: "value"
      }
    ],
    chartCursor: {
      categoryBalloonDateFormat: "YYYY",
      cursorAlpha: 0,
      valueLineEnabled: true,
      valueLineBalloonEnabled: true,
      valueLineAlpha: 0.5,
      fullWidth: true
    },
    dataDateFormat: "YYYY",
    categoryField: "year",
    categoryAxis: {
      gridPosition: "start",
      labelRotation: 45,
      minorGridEnabled: true,
      /* ENSURE 2 LINES BELOW ARE ADDED */
      autoGridCount: false,
      gridCount: 12
    },
    export: {
      enabled: false
    }
  });

  //Grafica 2
  var chart2 = AmCharts.makeChart("chartDosdiv", {
    type: "pie",
    theme: "light",
    dataProvider: dataJSON,
    valueField: "value",
    titleField: "year",
    startEffect: "elastic",
    startDuration: 2,
    labelRadius: 1,
    innerRadius: "65%",
    depth3D: 10,
    balloonText:
      "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
    angle: 10,
    export: {
      enabled: true
    }
  });

  //Grafica 3
  var chart3 = AmCharts.makeChart("chartTresdiv", {
    type: "serial",
    theme: "light",
    marginRight: 40,
    marginLeft: 40,
    autoMarginOffset: 20,
    dataDateFormat: "YYYY-MM-DD",
    valueAxes: [
      {
        id: "v1",
        axisAlpha: 0,
        position: "left",
        ignoreAxisWidth: true
      }
    ],
    balloon: {
      borderThickness: 1,
      shadowAlpha: 0
    },
    graphs: [
      {
        id: "g1",
        balloon: {
          drop: true,
          adjustBorderColor: false,
          color: "#ffffff",
          type: "smoothedLine"
        },
        fillAlphas: 0.2,
        bullet: "round",
        bulletBorderAlpha: 1,
        bulletColor: "#FFFFFF",
        bulletSize: 5,
        hideBulletsCount: 50,
        lineThickness: 2,
        title: "red line",
        useLineColorForBulletBorder: true,
        valueField: "value",
        balloonText: "<span style='font-size:18px;'>[[value]]</span>"
      }
    ],
    chartCursor: {
      valueLineEnabled: true,
      valueLineBalloonEnabled: true,
      cursorAlpha: 0,
      zoomable: false,
      valueZoomable: true,
      valueLineAlpha: 0.5
    },
    categoryField: "year",
    categoryAxis: {
      gridPosition: "start",
      labelRotation: 45,
      minorGridEnabled: true,
      /* ENSURE 2 LINES BELOW ARE ADDED */
      autoGridCount: false,
      gridCount: 12
    },
    export: {
      enabled: true
    },
    dataProvider: dataJSON
  });

  //Mapa
  var map = AmCharts.makeChart("mapdiv", {
    type: "map",
    theme: "light",
    projection: "miller",
    pathToImages: "http://www.amcharts.com/lib/3/images/",
    dataProvider: {
      map: "mexicoHigh",
      getAreasFromMap: true
    },
    areasSettings: {
      autoZoom: true,
      selectedColor: "#CC0000"
    },
    export: {
      enabled: true,
      position: "bottom-right",
      reviver: function(obj, svg) {
        var className = this.setup.chart.classNamePrefix + "-map-area";

        // ONLY APPLY ON MAP AREAS
        if (obj.classList.indexOf(className) != -1) {
          // GET AREA ID THROUGH CLASSNAME
          obj.id = obj.classList[1].split("-").pop();

          // BACKUP ORIGINAL TO SVG METHOD
          obj.toSVG_BACKUP = obj.toSVG;

          // BYPASS TOSVG TO INJECT ADDITIONAL PARAMETER
          obj.toSVG = function(reviver) {
            var string = this.toSVG_BACKUP.apply(this, arguments);
            var explode = string.split(" "); // quick and dirty
            var tag = explode.shift();

            // INJECT AREA ID
            explode.unshift('id="' + this.id + '"');

            // PLACE BACK THE TAG
            explode.unshift(tag);

            // MERGE IT; RETURN IT;
            string = explode.join(" ");

            return string;
          };
        }
      }
    }
  });

  socket.on("updateGrafica", function(data) {
    // remove datapoint from the beginning
    chart1.dataProvider.shift();
    chart2.dataProvider.shift();
    chart3.dataProvider.shift();

    numeroPrueba = data.year;

    chart1.dataProvider.push(data);
    chart2.dataProvider.push(data);
    chart3.dataProvider.push(data);

    chart1.validateData();
    chart2.validateData();
    chart3.validateData();
  }); //Fin del mensaje
});
