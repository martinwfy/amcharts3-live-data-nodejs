var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/',ensureAuthenticated ,function(req, res, next) {
    res.render('index', {'title':'Inicio'});
});

router.get('/ghome',ensureAuthenticated , function(req, res, next) {
    res.render('index', {'title':'Home'});
});
router.get('/greload',ensureAuthenticated , function(req, res, next) {
    res.render('reload', {'title':'Test'});
});

router.get('/gsearch',ensureAuthenticated , function(req, res, next) {
    res.render('search', {'title':'Taxt'});
});

router.get('/gstats',ensureAuthenticated , function(req, res, next) {
    res.render('gstats', {'title':'Taxt'});
});

router.get('/resizefull',ensureAuthenticated , function(req, res, next) {
    res.render('resizefull', {'title':'Taxt'});
});

function ensureAuthenticated(req,res,next){
    //Si esta logeado entramos al portal
    if(req.isAuthenticated()){
        return next();
    }
    //Si isAuthenticated = false regresamos al login
    res.redirect('/users/login');
}

module.exports = router;
