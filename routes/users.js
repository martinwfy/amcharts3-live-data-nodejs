var express = require('express');
var router = express.Router();

//Subir archivos
var multer = require('multer');
var upload = multer({dest:'./uploads'});

var User = require('../models/user');
var LocalStrategy = require('passport-local').Strategy;
var expressValidator = require('express-validator');
var passport = require('passport');


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/register', function(req, res, next) {
    res.render('register', {'title':'Register'});
});

router.post('/register', upload.single('profileimage'),function(req, res, next) {
    //console.log(req.body.name);
    var name=req.body.name;
    var email=req.body.email;
    var username=req.body.username;
    var password=req.body.password;
    var password2=req.body.password2;
    var profileImage;
    //La imagen se sube automaticamente
    if(req.file){
        profileImage = req.file.filename;
    }else{
        profileImage = 'noImage.jpg';
    }

    req.checkBody('name','Name is required').notEmpty();
    req.checkBody('email','Email is required').notEmpty();
    req.checkBody('email','Email is not valid').isEmail();
    req.checkBody('username','Username is required').notEmpty();
    req.checkBody('password','Password is required').notEmpty();
    req.checkBody('password2','Password do not match').equals(req.body.password);

    var errors = req.validationErrors();
    if(errors){
        res.render('register',{
            errors:errors
        });
    }else{
        var newUser = new User({
            name:name,
            email:email,
            username:username,
            password:password,
            profileImage:profileImage
        });

        User.createUser(newUser, function(error,user){
            if(error) throw error;
        });

        req.flash('success','You are now registered and can login');

        res.location('/');
        res.redirect('/');
    }
});


router.post('/login',passport.authenticate('local',
    {failureRedirect:'/users/login', failureFlash:'Invalid Username or Password'}),
    function(req,res){
        req.flash('success','You are now logged in');
        res.redirect('/');
    });



router.get('/login', function(req, res, next) {
    res.render('login', {'title':'Login'});
});

passport.serializeUser(function(user,done){
   done(null,user.id);
});

passport.deserializeUser(function(id,done){
   User.getUserById(id,function(err,user){
     done(err,user);
   });
});

passport.use(new LocalStrategy(function(username,password,done){
    User.getUserByUsername(username,function(err,user){
        if(err) throw err;
        if(!user){
            return done(null,false,{message:'Unknown User'});
        }

        User.comparePassword(password,user.password,function(err,isMatch){
            if(err) return done(err);
            if(isMatch){
                return done(null,user);
            }
            else{
                return done(null,false,{message:'Invalid Password'});
            }
        });
    });
}));

router.get('/logout',function(req,res){
   req.logout();
   req.flash('success','You are now logged out');
   res.redirect('/users/login');
});

module.exports = router;
