var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

//Sesiones
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var expressValidator = require('express-validator');

var multer = require('multer');
var flash = require('connect-flash');
var mongo = require('mongodb');
var mongoose = require('mongoose');
var upload = multer({dest:'./uploads'});
var db = mongoose.connection;

var bcrypt = require('bcryptjs');

var icons = require('glyphicons')

//Rutas
var index = require('./routes/index');
var users = require('./routes/users');
var menu = require('./routes/menu');

var http = require('http');
var app = express(),
    server = http.createServer(app) ,
    io = require('socket.io')(server);

var puerto = 9000;
var ip="localhost";
var urlServer='http://'+ip;

// view engine setup
app.set('port', process.env.PORT || puerto);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


//Sesiones
app.use(session({
    secret:'Secret',
    saveUninitialized:true,
    resave:true
}));

//Passport
app.use(passport.initialize());
app.use(passport.session());

//Validacion
app.use(expressValidator({
    errorFormatter: function(param, msg, value) {
        var   namespace = param.split('.'),
            root      = namespace.shift(),
            formParam = root;

        while(namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param : formParam,
            msg   : msg,
            value : value
        };
    }
}));

//Mensajes
app.use(require('connect-flash')());
app.use(function (req, res, next) {
    res.locals.messages = require('express-messages')(req, res);
    next();
});

//Obtener urls
app.get('*',function(req,res,next){
    res.locals.user = req.user || null;
    console.log('User: '+res.locals.user);
    next();
});

//Setear acceso a las rutas
app.use('/', index);
app.use('/users', users);
app.use('/app', menu);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', urlServer+":"+puerto);

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PATCH');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//Acciones del socket io
var ultimoAnio=1994;
io.sockets.on('connection', function(client) {
    console.log("Cliente: "+client.id);

    // when a 'send message' message is sent along with some data...
    client.on('test', function(data) {
        //Mensaje de regreso al cliente
        client.emit('regresoMensaje', "o/");
    });
    client.on('actualiza', function(numeroPrueba) {
        //Mensaje de regreso al cliente

        var visits =Math.floor(Math.random() * 550) + 500;
        numeroPrueba=numeroPrueba+1;
        var data={
            year: numeroPrueba,
            value: visits
        };
        client.emit('updateGrafica', data);
    });

});

module.exports = app;


//Iniciar servidor
server.listen(puerto, ip);